package system

import (
	"io"
	"os/exec"

	"github.com/sirupsen/logrus"
)

// Command reprensent a command output
type Command struct {
	Out string
	Err string
}

// RunCommand execute the executable with args and return an output
func RunCommand(executable string, args ...string) (Command, error) {
	logrus.WithField("executable", executable).Debug("running a new command")
	cmd := exec.Command(executable, args...)
	stderrPipe, err := cmd.StderrPipe()
	if err != nil {
		return Command{}, err
	}

	stdoutPipe, err := cmd.StdoutPipe()
	if err != nil {
		return Command{}, err
	}

	if cmdErr := cmd.Start(); cmdErr != nil {
		return Command{}, cmdErr
	}

	stderr, err := io.ReadAll(stderrPipe)
	if err != nil {
		return Command{}, err
	}

	stdout, err := io.ReadAll(stdoutPipe)
	if err != nil {
		return Command{}, err
	}

	if err := cmd.Wait(); err != nil {
		return Command{}, err
	}

	return Command{
		Out: string(stdout),
		Err: string(stderr),
	}, nil
}
