package system

import "runtime"

// IsWindows return true if the current platform is Windows 🤮
func IsWindows() bool {
	return runtime.GOOS == "windows"
}

// IsMac return true if the current platform is MAC
func IsMac() bool {
	return runtime.GOOS == "darwin"
}
