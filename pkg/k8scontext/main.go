package k8scontext

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"k8s.io/client-go/tools/clientcmd"
)

const devTunnelPort = "8888"
const proTunnelPort = "8889"

const bastionDev = "hosco-dev-bastion"
const bastionPro = "hosco-pro-bastion"

const pro = "pro"

func Get() (string, error) {
	kubeConfigPath := filepath.Join(
		os.Getenv("HOME"), ".kube", "config",
	)
	kubeConfig, err := clientcmd.LoadFromFile(kubeConfigPath)
	if err != nil {
		return "", errors.Wrapf(err, "could not load config")
	}
	return kubeConfig.CurrentContext, nil
}

func IsGKEContext(ctx string) bool {
	return strings.HasPrefix(ctx, "gke")
}

func getGKEenv(ctx string) string {
	return ctx[len(ctx)-3:]
}

func GetGKETunnelPort(ctx string) string {
	gkeCtxEnv := getGKEenv(ctx)
	if gkeCtxEnv == pro {
		return proTunnelPort
	}
	return devTunnelPort
}

func GetBastion(ctx string) string {
	gkeCtxEnv := getGKEenv(ctx)
	if gkeCtxEnv == pro {
		return bastionPro
	}
	return bastionDev
}

func IsContextValidForEnv(ctx string, env string) bool {
	gkeCtxEnv := getGKEenv(ctx)
	if gkeCtxEnv == pro {
		return false
	}
	var minikube = "minikube"
	return ctx == minikube && env == minikube || ctx != minikube && env != minikube
}
