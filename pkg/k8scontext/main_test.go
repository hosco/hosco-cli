package k8scontext

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsGKEContext(t *testing.T) {
	t.Parallel()
	assert.False(t, IsGKEContext("minikube"))
	assert.True(t, IsGKEContext("gke_hosco-1524665240446_europe-west3_hosco-pro"))
	assert.True(t, IsGKEContext("gke_hosco-1524665240446_europe-west3_hosco-dev"))
}

func TestIsContextValidForEnv(t *testing.T) {
	t.Parallel()
	assert.True(t, IsContextValidForEnv("minikube", "minikube"))
	assert.False(t, IsContextValidForEnv("minikube", "sandbox"))
	assert.True(t, IsContextValidForEnv("gke_hosco-1524665240446_europe-west3_hosco-dev", "sandbox"))
	assert.False(t, IsContextValidForEnv("gke_hosco-1524665240446_europe-west3_hosco-dev", "minikube"))
	assert.False(t, IsContextValidForEnv("gke_hosco-1524665240446_europe-west3_hosco-pro", "minikube"))
	assert.False(t, IsContextValidForEnv("gke_hosco-1524665240446_europe-west3_hosco-pro", "sandbox"))

}
