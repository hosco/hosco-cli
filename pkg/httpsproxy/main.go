package httpsproxy

import (
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	context "gitlab.com/hosco/hosco-cli/pkg/k8scontext"
)

const httpsProxyVar = "HTTPS_PROXY"

func setEnvVarGKE(ctx string) {
	httpsProxy := os.Getenv(httpsProxyVar)

	tunnelPort := context.GetGKETunnelPort(ctx)
	if strings.HasSuffix(httpsProxy, tunnelPort) {
		logrus.Debugf("correct value for HTTPS_PROXY: %s", httpsProxy)
		return
	}
	newHTTPSProxy := fmt.Sprintf("localhost:%s", tunnelPort)
	os.Setenv(httpsProxyVar, newHTTPSProxy)
	logrus.Debugf("HTTPS_PROXY value was '%s', set correctly to %s", httpsProxy, newHTTPSProxy)
}

func SetEnvVar(ctx string) {
	logrus.Debugf("checking HTTPS_PROXY value for %s", ctx)
	httpsProxy := os.Getenv(httpsProxyVar)
	if context.IsGKEContext(ctx) {
		setEnvVarGKE(ctx)
		return
	}
	if httpsProxy != "" {
		os.Unsetenv(httpsProxyVar)
		logrus.Debugf("value for HTTPS_PROXY was %s , unseted correctly", httpsProxy)
		return
	}
	logrus.Debugf("HTTPS_PROXY is not set, so everything is ok")
}

func PrintSetEnvCommand() {
	httpsProxy := os.Getenv(httpsProxyVar)
	//nolint:forbidigo // print allowed
	fmt.Printf("export %s=%s", httpsProxyVar, httpsProxy)
}
