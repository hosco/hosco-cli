package k8s

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/hosco/hosco-cli/pkg/httpsproxy"
	"gitlab.com/hosco/hosco-cli/pkg/k8scontext"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"

	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth" // need for GCP auth
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/portforward"
	"k8s.io/client-go/transport/spdy"
)

// Client is a simplified kubernetes client
type Client struct {
	k8sCli *kubernetes.Clientset
	config *rest.Config
}

// NewClient create a new kubernetes client
func NewClient() (*Client, error) {
	kubeconfig := filepath.Join(
		os.Getenv("HOME"), ".kube", "config",
	)
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, errors.New("couldn't build the kubernetes configuration")
	}

	k8sCli, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't create the kubernetes client")
	}

	return &Client{
		k8sCli: k8sCli,
		config: config,
	}, nil
}

func (cli Client) isConnectionOk() bool {
	logrus.Debug("testing connection")
	_, err := cli.k8sCli.DiscoveryClient.ServerVersion()
	if err != nil {
		logrus.Debug(err)
	}
	return err == nil
}

func (cli Client) SetTunnelIfNeeded(ctx string) error {
	httpsproxy.SetEnvVar(ctx)
	if !k8scontext.IsGKEContext(ctx) {
		logrus.Debug("no need to test tunnel: not in gke context")
		return nil
	}

	if cli.isConnectionOk() {
		logrus.Debug("connection is ok")
		return nil
	}

	tunnelPort := k8scontext.GetGKETunnelPort(ctx)
	bastion := k8scontext.GetBastion(ctx)

	logrus.Debugf("Connection failed. Removing idle tunnels for port %s", tunnelPort)

	getTunnelPIDsCmd := fmt.Sprintf(
		"ps aux | grep ssh | grep \"%s:localhost:8888 -N\" | awk {'print $2'} | xargs", tunnelPort,
	)
	cmd := exec.Command("bash", "-c", getTunnelPIDsCmd)
	o, cmdErr := cmd.Output()
	if cmdErr != nil {
		return cmdErr
	}

	ids := strings.Split(strings.Trim(string(o), "\n"), " ")

	for _, strid := range ids {
		id, err := strconv.Atoi(strid)
		if err != nil {
			logrus.WithError(err).Debugf("could not convert %s to int", strid)
			continue
		}

		proc, err := os.FindProcess(id)
		if err != nil {
			logrus.WithError(err).Debugf("could not find process %d", id)
			continue
		}
		err = proc.Kill()
		if err != nil {
			logrus.WithError(err).Debugf("could not kill process %d", id)
		}
	}

	logrus.Debugf("Cleaned idle tunnels. Setting tunnel for port %s in %s", tunnelPort, bastion)

	gcloudCommand := fmt.Sprintf(
		"unset HTTPS_PROXY && gcloud compute ssh %s --tunnel-through-iap -- -L %s:localhost:8888 -N -q -f",
		bastion,
		tunnelPort,
	)
	cmd = exec.Command("bash", "-c", gcloudCommand)
	err := cmd.Run()
	if err != nil {
		return err
	}
	logrus.Debug("Tunnel set correctly")
	return nil
}

// Namespaces return all namespaces for the current context
func (cli Client) Namespaces() ([]string, error) {
	api := cli.k8sCli.CoreV1()

	ns, err := api.Namespaces().List(context.TODO(), metav1.ListOptions{})

	if err != nil {
		logrus.WithError(err).Error("couldn't retrieve namespace")
		return nil, errors.New("couldn't retrieve namespace")
	}

	namespaces := []string{}
	for _, item := range ns.Items {
		namespaces = append(namespaces, item.Name)
	}

	return namespaces, nil
}

// DeploymentToPodName return the pod name associated to the deployment
func (cli Client) DeploymentToPodName(namespace string, deploymentName string) (v1.Pod, error) {
	deployment, err := cli.k8sCli.AppsV1().Deployments(namespace).Get(context.TODO(), deploymentName, metav1.GetOptions{})
	if err != nil {
		logrus.WithField("deploymentName", deploymentName).WithError(err).Error("couldn't find a deployment")
		return v1.Pod{}, err
	}

	set := labels.Set(deployment.Spec.Selector.MatchLabels)
	listOptions := metav1.ListOptions{LabelSelector: set.AsSelector().String()}
	pods, err := cli.k8sCli.CoreV1().Pods(namespace).List(context.TODO(), listOptions)
	if err != nil {
		logrus.
			WithField("deploymentName", deploymentName).
			WithError(err).
			Error("couldn't find pods associated to the deployment")
		return v1.Pod{}, err
	}

	const onePod = 1
	if len(pods.Items) != onePod {
		logrus.WithField("len", len(pods.Items)).Debug("Found more than one pod")
		return v1.Pod{}, fmt.Errorf("found 0 or more than one pod for the deployment %s", deployment.Name)
	}

	logrus.WithField("podName", pods.Items[0].Name).Debug("Found exactly one pod")
	return pods.Items[0], nil
}

// PortForward a pod locally on the port specified
func (cli Client) PortForward(pod v1.Pod, port int) error {
	logrus.
		WithField("namespace", pod.Namespace).
		WithField("name", pod.Name).
		WithField("status", pod.Status.Message).
		Debug("Pod found")

	req := cli.k8sCli.RESTClient().Post().
		Resource("pods").
		Namespace(pod.Namespace).
		Name(pod.Name).
		SubResource("portforward")

	portForwardURL := req.URL()
	portForwardURL.Path = fmt.Sprintf("/api/v1%s", portForwardURL.Path) // Need to add this prefix to the URL manually

	transport, upgrader, err := spdy.RoundTripperFor(cli.config)
	if err != nil {
		logrus.WithError(err).Error("couldn't prepare the transport connection")
		return err
	}

	stopChannel := make(chan struct{}, 1)
	readyChannel := make(chan struct{})

	dialer := spdy.NewDialer(upgrader, &http.Client{Transport: transport}, http.MethodPost, portForwardURL)
	fw, err := portforward.NewOnAddresses(
		dialer,
		[]string{"0.0.0.0"},
		[]string{fmt.Sprintf("%d:%d", port, port)},
		stopChannel,
		readyChannel,
		os.Stdout,
		os.Stderr,
	)
	if err != nil {
		logrus.WithError(err).Error("couldn't create the port forward")
		return err
	}

	logrus.Debug("Start the portforward")
	return fw.ForwardPorts()
}
