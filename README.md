# hosco-cli

The cli tool that dev can use @Hosco

## Development

You can run the project locally with `go run main.go help` or `go run main.go k8s` or `go run main.go k8s forward`.

You can enable the verbose mode with `go run main.go --verbose k8s forward`.

Remember to increase the version in root.go (try to keep it aligned with tag release).
## Install it

You can install it with `go build main.go && mv main /usr/local/bin/hosco`

## Use it

### Checking current context and namespaces availale

```
hosco k8s context
```

### Port forwarding

You can run a port-forward, provided that you are in the correct context.

E.g.: in dev `kubectx gke_hosco-1524665240446_europe-west3_hosco-dev`

```
hosco k8s forward sandbox
```

In minikube `kubectx minikube`

```
hosco k8s forward minikube
```

### Pre-connecting to a cluster

This command sets everything to be able to connect to a cluster in GCP (dev or pro).
You must however be in the proper context. E.g: `kubectx gke_hosco-1524665240446_europe-west3_hosco-dev`

```
$(hosco k8s context pre-connect)
```

The command sets a tunnel if it is needed and prints a command that sets an env variable, `export HTTPS_PROXY=localhost:8888`

That's why running it with `$(...)` the output is run. For practicity, it can be run like this:

```
kubectx gke_hosco-1524665240446_europe-west3_hosco-dev && $(hosco k8s context pre-connect)
k9s
```

For pro:

```
kubectx gke_hosco-1524665240446_europe-west3_hosco-pro && $(hosco k8s context pre-connect)
k9s
```
