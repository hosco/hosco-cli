package confluence

import (
	"github.com/sirupsen/logrus"
	"github.com/skratchdot/open-golang/open"
	"github.com/spf13/cobra"
)

// InitCmd creates the command and assigns the command to the root command
func InitCmd(root *cobra.Command) error {
	command := cobra.Command{
		Use:   "confluence",
		Short: "open confluence homepage",
		Run: func(cmd *cobra.Command, args []string) {
			if err := open.Run("https://hoscojira.atlassian.net/wiki/spaces/HOS/overview"); err != nil {
				logrus.WithError(err).Error("couldn't open confluence homepage")
			}
		},
	}

	command.AddCommand(&cobra.Command{
		Use:   "operations",
		Short: "open confluence operations",
		Run: func(cmd *cobra.Command, args []string) {
			if err := open.Run("https://hoscojira.atlassian.net/wiki/spaces/HOS/pages/3440652/Operations"); err != nil {
				logrus.WithError(err).Error("couldn't open confluence operations")
			}
		},
	})

	command.AddCommand(&cobra.Command{
		Use:   "microservices",
		Short: "open confluence microservices",
		Run: func(cmd *cobra.Command, args []string) {
			if err := open.Run("https://hoscojira.atlassian.net/wiki/spaces/HOS/pages/19038229/Microservices"); err != nil {
				logrus.WithError(err).Error("couldn't open confluence microservices")
			}
		},
	})

	command.AddCommand(&cobra.Command{
		Use:   "faq",
		Short: "open confluence faq",
		Run: func(cmd *cobra.Command, args []string) {
			if err := open.Run("https://hoscojira.atlassian.net/wiki/spaces/HOS/pages/278331437/Hosco+Dev+F.A.Q."); err != nil {
				logrus.WithError(err).Error("couldn't open confluence faq")
			}
		},
	})

	root.AddCommand(&command)
	return nil
}
