package open

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/skratchdot/open-golang/open"
	"github.com/spf13/cobra"
	"gitlab.com/hosco/hosco-cli/cmd/open/confluence"
)

// InitCmd creates the command and assigns the command to the root command
func InitCmd(root *cobra.Command) error {
	command := &cobra.Command{
		Use:   "open",
		Short: "general shortcut",
		Long:  `open perform action to open the browser`,
		Run: func(cmd *cobra.Command, args []string) {
			//nolint:forbidigo // print allowed
			fmt.Println("open missing the target")
		},
	}

	if err := confluence.InitCmd(command); err != nil {
		logrus.WithError(err).Error("couldn't create confluence command")
		return err
	}

	command.AddCommand(&cobra.Command{
		Use:   "gitlab",
		Short: "open gitlab todos",
		Run: func(cmd *cobra.Command, args []string) {
			if err := open.Run("https://gitlab.com/dashboard/todos"); err != nil {
				logrus.WithError(err).Error("couldn't open gitlab")
			}
		},
	})

	command.AddCommand(&cobra.Command{
		Use:   "jira",
		Short: "open jira",
		Run: func(cmd *cobra.Command, args []string) {
			if err := open.Run("https://hoscojira.atlassian.net/jira/your-work"); err != nil {
				logrus.WithError(err).Error("couldn't open jira")
			}
		},
	})

	root.AddCommand(command)
	return nil
}
