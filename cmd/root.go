package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hosco/hosco-cli/cmd/k8s"
	"gitlab.com/hosco/hosco-cli/cmd/open"
	"gitlab.com/hosco/hosco-cli/cmd/vpn"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

// ignoring lint for globals: it's needed to work with Cobra

//nolint:gochecknoglobals // old
var cfgFile string

//nolint:gochecknoglobals // old
var verbose bool

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	root := &cobra.Command{
		Use:     "hosco",
		Short:   "command line interface for local environment workflow at Hosco",
		Long:    `command line interface for local environment workflow at Hosco. e.g.: to list current context namespaces`,
		Version: "v2.0.0",
	}

	if err := k8s.InitCmd(root); err != nil {
		logrus.WithError(err).Fatal("couldn't init the kubernetes command")
	}

	if err := open.InitCmd(root); err != nil {
		logrus.WithError(err).Fatal("couldn't init the open command")
	}

	if err := vpn.InitCmd(root); err != nil {
		logrus.WithError(err).Fatal("couldn't init the vpn command")
	}

	root.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.hosco-cli.yaml)")
	root.PersistentFlags().BoolVar(&verbose, "verbose", false, "verbose mode (debug and info)")

	cobra.OnInitialize(initVerboseMode)
	cobra.OnInitialize(initConfig)

	if err := root.Execute(); err != nil {
		logrus.WithError(err).Fatal("couldn't run command")
	}
}

func initVerboseMode() {
	if verbose {
		logrus.Info("verbose mode detected")
		logrus.SetLevel(logrus.DebugLevel)
	} else {
		logrus.SetLevel(logrus.ErrorLevel)
	}
}

func initConfig() {
	logrus.WithField("cfgFile", cfgFile).Debug("config file value")

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			logrus.WithError(err).Fatal("couldn't init the configuration")
		}

		// Search config in home directory with name ".hosco-cli" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".hosco-cli")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		logrus.WithField("path", viper.ConfigFileUsed()).Debug("using config file")
	}
}
