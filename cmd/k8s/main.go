package k8s

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hosco/hosco-cli/cmd/k8s/context"
	"gitlab.com/hosco/hosco-cli/cmd/k8s/forward"
)

// InitCmd creates the command and assigns the command to the root command
func InitCmd(root *cobra.Command) error {
	command := &cobra.Command{
		Use:   "k8s",
		Short: "k8s related commands",
		Long:  `k8s performs actions related to k8s`,
		Run: func(cmd *cobra.Command, args []string) {
			logrus.Error("k8s missing command or flag, try: `hosco k8s -h`")
		},
	}

	if err := context.InitCmd(command); err != nil {
		logrus.WithError(err).Error("couldn't create context command")
		return err
	}

	if err := forward.InitCmd(command); err != nil {
		logrus.WithError(err).Error("couldn't create forward command")
		return err
	}

	root.AddCommand(command)
	return nil
}
