package context

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hosco/hosco-cli/pkg/httpsproxy"
	"gitlab.com/hosco/hosco-cli/pkg/k8s"
	"gitlab.com/hosco/hosco-cli/pkg/k8scontext"
)

// InitCmd creates the command and assigns the command to the root command
func InitCmd(root *cobra.Command) error {
	cmd := cobra.Command{
		Use:   "context",
		Short: "lists the namespaces and the current context",
		Long:  `lists the namespaces and the current context`,
		Run: func(cmd *cobra.Command, args []string) {
			printContextAndNamespaces()
		},
	}
	cmd.AddCommand(&cobra.Command{
		Use:   "pre-connect",
		Short: "pre-connect to current context",
		Long:  `sets the tunnel and prints env variable`,
		Run: func(cmd *cobra.Command, args []string) {
			printSetEnvCommand()
		},
	})
	root.AddCommand(&cmd)

	return nil
}

func printSetEnvCommand() {
	logrus.Debug("print export env command")
	ctx, err := k8scontext.Get()
	if err != nil {
		logrus.Error(err)
	}
	cli, err := k8s.NewClient()
	if err != nil {
		logrus.WithError(err).Error("couldn't create the kubernetes client")
		return
	}
	err = cli.SetTunnelIfNeeded(ctx)
	if err != nil {
		logrus.WithError(err).Error("couldn't set tunnel")
		return
	}

	httpsproxy.PrintSetEnvCommand()
}

func printContextAndNamespaces() {
	logrus.Debug("display current context and all its namespaces")

	ctx, err := k8scontext.Get()
	if err != nil {
		logrus.Error(err)
	}

	//nolint:forbidigo // print allowed
	fmt.Printf("Current context: %s\n", ctx)
	//nolint:forbidigo // print allowed
	fmt.Println("\n----")
	cli, err := k8s.NewClient()
	if err != nil {
		logrus.WithError(err).Error("couldn't create the kubernetes client")
		return
	}

	err = cli.SetTunnelIfNeeded(ctx)
	if err != nil {
		logrus.WithError(err).Error("couldn't set tunnel")
		return
	}

	//nolint:forbidigo // print allowed
	fmt.Println("Namespaces:")
	namespaces, err := cli.Namespaces()
	if err != nil {
		logrus.WithError(err).Error("couldn't retrieve all namespaces")
		return
	}

	for _, namespace := range namespaces {
		//nolint:forbidigo // print allowed
		fmt.Println(namespace)
	}
}
