package forward

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hosco/hosco-cli/pkg/k8s"
	"gitlab.com/hosco/hosco-cli/pkg/k8scontext"
)

// InitCmd creates the command and assigns the command to the root command
func InitCmd(root *cobra.Command) error {
	command := &cobra.Command{
		Use:   "forward",
		Short: "port forward ElasticSearch and Postgres locally",
		Long: `port forward
			- ElasticSearch on port 9200
			- Postgresql on port 5432
			on this device. Specify the dev env (for example minikube, sandbox, pasta, pizza, ...)`,
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			portForwardADevEnv(args[0])
		},
	}

	root.AddCommand(command)
	return nil
}

func getEnvAndNamespace(env string) (string, string) {
	// minikube is a special case as we don't use helm to deploy in minikube
	namespace := "web"
	if env == "minikube" {
		logrus.Debug("port forward minikube")
		env = "web-hoscov2"
		namespace = "default"
	} else {
		env = fmt.Sprintf("web-%s-hoscov2", env)
	}
	return env, namespace
}

func setPortForward(resource string, port int, namespace string, env string, cli *k8s.Client) error {
	pod, err := cli.DeploymentToPodName(namespace, fmt.Sprintf("%s-%s", env, resource))
	if err != nil {
		return errors.Wrapf(err, "couldn't convert deployment %s to pod", resource)
	}
	logrus.WithField("name", pod.Name).Debugf("%s pod found", resource)
	err = cli.PortForward(pod, port)
	if err != nil {
		return err
	}
	return nil
}

func portForwardADevEnv(env string) {
	logrus.Debug("port forward a dev env")

	ctx, err := k8scontext.Get()
	if err != nil {
		logrus.Error(err)
	}

	if !k8scontext.IsContextValidForEnv(ctx, env) {
		//nolint:forbidigo // print allowed
		fmt.Printf("wrong context: %s. Please change to the right context with kubectx", ctx)
		return
	}

	env, namespace := getEnvAndNamespace(env)

	cli, err := k8s.NewClient()
	if err != nil {
		logrus.WithError(err).Error("couldn't create the kubernetes client")
		return
	}

	err = cli.SetTunnelIfNeeded(ctx)
	if err != nil {
		logrus.WithError(err).Error("couldn't set tunnel")
		return
	}

	ch := make(chan bool)
	go func() {
		err = setPortForward("elasticsearch", 9200, namespace, env, cli)
		if err != nil {
			logrus.WithError(err).Error("couldn't port forward the elasticsearch dev env")
			ch <- false
			return
		}
		ch <- true
	}()

	go func() {
		err = setPortForward("postgresql", 5432, namespace, env, cli)
		if err != nil {
			logrus.WithError(err).Error("couldn't port forward the postgres dev env")
			ch <- false
			return
		}
		ch <- true
	}()

	if !<-ch {
		logrus.Error("couldn't portforward a dev env...")
	}
}
