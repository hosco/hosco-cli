package vpn

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/hosco/hosco-cli/pkg/system"
)

const ovpncliPath = "/Library/Frameworks/OpenVPN.framework/Versions/Current/bin/ovpncli"

// InitCmd creates the command and assigns the command to the root command
func InitCmd(root *cobra.Command) error {
	command := &cobra.Command{
		Use:   "vpn",
		Short: "vpn switch",
		Long: `vpn performs actions to switch from one vpn to another one.
			You can use disconnect to disconnect the current VPN.
			You can use status to display the current status.`,
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if system.IsWindows() {
				logrus.Error("you can maybe start using a good OS ?")
				return
			} else if !system.IsMac() {
				logrus.Error("this command works only on mac platform")
				return
			}

			if args[0] == "disconnect" {
				disconnect()
				return
			}

			if args[0] == "status" {
				status()
				return
			}

			change(args[0])
		},
	}

	root.AddCommand(command)
	return nil
}

func disconnect() {
	command, err := system.RunCommand(ovpncliPath, "disconnect")
	if err != nil {
		logrus.WithError(err).Error("couldn't execute the disconnect command")
	}

	displayCommand(command)
}

func status() {
	command, err := system.RunCommand(ovpncliPath, "status")
	if err != nil {
		logrus.WithError(err).Error("couldn't execute the status command")
	}

	displayCommand(command)
}

func change(name string) {
	commandEnum, cmdErr := system.RunCommand(ovpncliPath, "enum")
	if cmdErr != nil {
		logrus.WithError(cmdErr).Error("couldn't execute the enum command")
	}

	logrus.WithField("out", commandEnum.Out).WithField("err", commandEnum.Err).Debug("enum output")
	var enum []struct {
		Host string `json:"host"`
	}

	if err := json.Unmarshal([]byte(commandEnum.Out), &enum); err != nil {
		logrus.WithError(err).Error("couldn't extract the enum command")
	}

	logrus.WithField("len", len(enum)).Debug("len of vpn")
	var host string

	for _, vpn := range enum {
		if strings.Contains(vpn.Host, name) {
			if host != "" {
				logrus.WithFields(logrus.Fields{
					"first":  host,
					"second": vpn.Host,
				}).Error("couldn't find only one vpn, please be more precise")
				return
			}

			host = vpn.Host
		}
	}

	if host == "" {
		logrus.Error("couldn't find a vpn")
	}

	commandConnect, err := system.RunCommand(ovpncliPath, "--target", host, "--allow_universal", "--disconnect", "connect")
	if err != nil {
		logrus.WithError(err).Error("couldn't execute the connect command")
	}

	displayCommand(commandConnect)
}

func displayCommand(command system.Command) {
	//nolint:forbidigo // print allowed
	fmt.Print(command.Out)

	if command.Err != "" {
		//nolint:forbidigo // print allowed
		fmt.Print(command.Err)
	}
}
